Actividad Adicional por Antonio Alvarez            09 Septiembre de 2019

1. Crear un repositorio con el nombre practicaQA y compartirlo a MTellez, BVarela
2. En rama master, editar el archivo “README.md”, colocando estas instrucciones de la actividad, subir en un commit que tenga como comentario “Inicio de Repositorio practicaQA” 
3. Crear dos ramas: sqa-pruebas  y  dev-desarrollo, a partir de master
4. Verificar que actualmente existirán tres ramas en total: master, sqa-pruebas  y  dev-desarrollo
5. master (es la predeterminada cuando crean el repositorio) debe contener únicamente el commit del paso 2 
6. Validar que existe la rama master (sección commits) 
7. Validar que existe la rama sqa-pruebas (sección commits) 
8. Validar que existe la rama dev-desarrollo (sección commits) 
9. Meter cambios 2 en sqa-pruebas  y 3 en dev-desarrollo , y subir sus commits
10. Hacer Merge en sqa-pruebas de la rama dev-desarrollo; validar que sqa-pruebas ya contenga todo lo de dev-desarrollo; posteriormente borrar rama  dev-desarrollo  y generar TAG 0.1.0 
11. Subir dos commits en blanco en la rama sqa-pruebas, con un emoji bajo la siguiente estructura: emojis 
 Commit 1: git commit --allow-empty  -m ":emoji: VT-1170 #comment Práctica terminada por   
 Esmeralda";
 Commit 2: git commit --allow-empty -m  ":emoji: VT-1170 #time 5h #comment Práctica realizada con  
 éxito";
12. Hacer Merge en Master de la rama sqa-pruebas  y mantener la rama sqa-pruebas (NO borrar) y generar TAG 0.2.0 
13. Ingresar por cada punto las imágenes que sean necesarias de evidencia de los pasos de esta actividad.
14. Al finalizar cada punto ir etiquetando a mtellez@artificesweb.com y bvarela@artificesweb.com con el comentario “He concluido el paso número XX , Saludos” (ver ejemplo en título de la actividad)

